class conversion() :
	def __init__( self ) :
		self.binare = "" ;

	def convertInOctet( self , n ) :
		if n == 0 :
			return '00000000' ;
		
		res = '' ;
		
		while n != 0 :
			n, res = n >> 1 , '1' + res ;
		self.binare = res ;
		
		res = '' ;

		if len(self.binare) < 8 :
			for x in xrange( 0, 8 - len( self.binare ) ):
				res = res + '0'
			res = res + self.binare ;
			self.binare = res ;
		return self.binare ;