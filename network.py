#!/usr/bin/python3

from client import * ;
from conversion import * ;
from ipaddress import * ;
import ipaddress ;
##from python_arptable import * ;
import os ;

class network() :
	def __init__(self):
		self.client = client() ;
		self.addrs = [] ;
		self.inet = [] ;
		self.mask_octet = "" ;
		self.addresse = "" ;
		self.table_arp = [] ;
		self.table_arp.append({"ip":"","mac":""}) ;

	def getMask( self ) :
		self.addrs = netifaces.ifaddresses( self.client.getNetworkInterface() ) ;
		self.inet = self.addrs[netifaces.AF_INET] ;
		return self.inet[0]['netmask'] ;

	def getMaskOctet( self ) :
		mask = self.getMask().split('.') ;
		convert = conversion() ;
		self.mask_octet = "" ;
		
		for octet in mask :
			self.mask_octet = self.mask_octet + convert.convertInOctet( int(octet) ) + '.' ;

		self.mask_octet = self.mask_octet[ 0 : len( self.mask_octet ) -1 ] ;
		return self.mask_octet ;

	def getMaskCiddr( self ) :
		return len(self.getMaskOctet().split('1') ) - 1 ;

	def getAddresse( self ) :
		ifc = IPv4Interface( str(self.client.getIp()) + "/" + str( self.getMaskCiddr() ) ) ;
		self.addresse = ifc.network ;
		return self.addresse ;

	def getListClients( self ) :
		##plageAdresse = ipaddress.ip_network( self.getAddresse() ).hosts() ;
		ether = Ether(dst="ff:ff:ff:ff:ff:ff") ;
		arp = ARP(pdst = str(self.getAddresse()) ) ;
			
		answer, unanswered = srp(ether/arp, retry=3 ,timeout = 10, iface = str(self.client.getNetworkInterface()), inter = 0) ;

		for sent, received in answer:
			if( str(received[0][1][ARP].psrc) != self.client.getDefaultRoute() ) :
				self.table_arp.append( { 'ip': str(received[0][1][ARP].psrc) , 'mac': str(received[0][1][ARP].hwsrc) } ) ;
		
		return self.table_arp ;