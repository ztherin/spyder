from scapy.all import * ;
import netifaces ;

class client() :
	def __init__( self ) :
		self.ip = "" ;
		self.mac = "" ;
		self.networkInterface = "" ;
		self.defaultRoute = "" ;

	def getIp( self ) :
		self.ip = IP(dst="0.0.0.0").src ;
		return self.ip ;

	def getMac( self ) :
		self.mac = Ether().src ;
		return self.mac ;

	def getNetworkInterface( self ) :
		gws = netifaces.gateways() ;
		self.networkInterface = gws['default'][netifaces.AF_INET] ;
		return self.networkInterface[1] ;

	def getDefaultRoute( self ) :
		gws = netifaces.gateways() ;
		self.defaultRoute = gws['default'][netifaces.AF_INET] ;
		return self.defaultRoute[0] ;

#client = client() ;
#print( client.getDefaultRoute() ) ;
